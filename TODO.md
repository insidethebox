
- fence and wall sounds are bugged due to high collision box

* lava bubble sound missing

- bad sounds:
* glass footstep sound
* gravel sounds 'wet'
* snow footsteps: remove high freq?

- nodes do not make a sound when falling.

- check placement code for all breakable nodes and make sure they can't be arbitrarily placed.
- screwdriver
- block modifiers (anything that does set_ remove_ or swap_ *node)

- light level in entry/exit lobbies is sometimes bugged at max light

- chat text color filtering is broken?

- tutorial: part chooser once completed

- spurious placeholders?

- terminal: move content to mod_storage

- terminal: make more useful (box/player stats?)

- better fix falling through lobby at box entry

- fix node names through out the entire node table.

- sticky (non-jump) nodes

- Spikey blocks?

- series maintenance GUI

- pick better understandable icons

- support for "patching" boxes - have other players submit newer versions for review?!

- build time in inventory

- teleports: modify attributes with a gui

- winter vegetation/grass/weed

- skybox-related ambiance sounds:
  - night: trunk -> owl
  - grass: windy -> wind sounds
           dark and stormy -> perhaps rain drop sounds?
  - water: sloshing lake wave?
  - sunny: tree leaves & cicadas

- swimming sounds

