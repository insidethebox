
--[[

  announce mod for minetest

  Copyright (C) 2019 Auke Kok <sofar@foo-projects.org>

  Permission to use, copy, modify, and/or distribute this software for
  any purpose with or without fee is hereby granted, provided that the
  above copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
  SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
  OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

]]--

announce = {}

function announce.all(msg)
	minetest.log("action", msg)
	minetest.chat_send_all(msg)
	if irc then
		irc.say(msg)
	end
end

function announce.player(name, msg)
	minetest.log("action", "announce to " .. name .. ": " .. msg)
	minetest.chat_send_player(name, msg)
	if irc and irc.joined_players[name] then
		irc.say(name, msg)
	end
end

function announce.admins(msg)
	minetest.log("action", "admin announce: " .. msg)
	if not irc then
		return
	end
	local irclist = minetest.settings:get("announce_irc_admins")
	if irclist == "" then
		return
	end
	local admins = string.split(string.gsub(irclist, " ", ""), ",")
	for _, name in pairs(admins) do
		if irc.joined_players[name] then
			irc.say(name, msg)
		end
	end
end
