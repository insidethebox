
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

--
-- Box score recording
--

-- [type] = limit
--  type: the string used for this type
--  limit: how many scores of these per player/box combo can be recorded
local score_types = {
	["buildtime"] = 1,
	["rating"] = 1,
	["category"] = 3,
	["time"] = 0,
	["damage"] = 0,
	["deaths"] = 0,
}

function boxes.score(name, box_id, score_type, values)
	assert(name)
	assert(type(name) == "string")
	assert(box_id)
	assert(score_types[score_type])
	assert(values)
	assert(type(values) == "table")

	local player_id = db.player_get_id(name)

	if minetest.check_player_privs(name, "review") then
		return
	end
	local bmeta = db.box_get_meta(box_id)
	if bmeta.meta.status ~= db.STATUS_ACCEPTED then
		return
	end
	if bmeta.meta.builder == name then
		return
	end

	db.player_points(player_id, box_id, score_type, values, score_types[score_type])
end

-- migrate old scores to new scoring format
minetest.after(5, function()
	for id = 0, db.get_last_box_id() do
		local bmeta = db.box_get_meta(id)
		if bmeta and bmeta.meta and bmeta.meta.completed_players then
			minetest.log("action", "migrating score data for box " .. id)
			for k, _ in pairs(bmeta.meta.completed_players) do
				-- migrate deaths, damage, time
				bmeta.meta.completed_players[k] = nil

				local time = bmeta.meta.time.player_best[k]
				boxes.score(k, id, "time", {time})
				bmeta.meta.time.player_best[k] = nil

				local damage = bmeta.meta.damage.player_best[k]
				boxes.score(k, id, "damage", {damage})
				bmeta.meta.damage.player_best[k] = nil

				local deaths = bmeta.meta.deaths.player_best[k]
				boxes.score(k, id, "deaths", {deaths})
				bmeta.meta.deaths.player_best[k] = nil
			end
			-- erase tables themselves
			bmeta.meta.time.player_best = nil
			bmeta.meta.damage.player_best = nil
			bmeta.meta.deaths.player_best = nil
			bmeta.meta.completed_players = nil
			-- store pruned meta
			db.box_set_meta(id, bmeta)
		end
	end
end)


