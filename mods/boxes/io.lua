
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

local function bytes_to_string(bytes)
	local s = {}
	for i = 1, #bytes do
		s[i] = string.char(bytes[i])
	end
	return table.concat(s)
end

function boxes.import_box(id, box_data)
	local data_index = 1
	local function read_u8()
		local c = string.byte(box_data, data_index)
		data_index = data_index + 1
		return c
	end
	local function read_u16()
		local a = read_u8()
		local b = read_u8()
		return 256 * a + b
	end
	local function read_pos()
		local x = read_u16()
		local y = read_u16()
		local z = read_u16()
		return {x = x, y = y, z = z}
	end
	local version = read_u8()
	assert (version == 1)
	local type = read_u8()
	local size = read_pos()
	local entry = read_pos()
	local exit = read_pos()
	db.box_set_data(id, string.sub(box_data, data_index))
	db.box_set_meta(id, {
		type = type,
		meta = {
			size = size,
			entry = entry,
			exit = exit,
		}
	})
end

function boxes.export_box(id)
	local data_index = 1
	local data = {}
	local function write_u8(x)
		data[data_index] = x
		data_index = data_index + 1
	end
	local function write_u16(x)
		write_u8(math.floor(x / 256))
		write_u8(x % 256)
	end
	local function write_pos(p)
		write_u16(p.x)
		write_u16(p.y)
		write_u16(p.z)
	end
	local meta = db.box_get_meta(id)
	write_u8(1) -- version
	write_u8(meta.type)
	write_pos(meta.meta.size)
	write_pos(meta.meta.entry)
	write_pos(meta.meta.exit)
	return bytes_to_string(data) .. db.box_get_data(id)
end

local function read_file(filename)
	local file = io.open(filename, "r")
	if file ~= nil then
		local file_content = file:read("*all")
		io.close(file)
		return file_content
	end
	return ""
end

--[[ -- kept for historical purposes.
local function write_file(filename, data)
	local file, err = io.open(filename, "w")
	if file then
		file:write(data)
		io.close(file)
	else
		error(err)
	end
end
]]--

local modpath = minetest.get_modpath(minetest.get_current_modname())

if not db.box_exists(0) then
	local box_data = read_file(modpath .. "/entry.box")
	boxes.import_box(0, box_data)
end

if not db.box_exists(1) then
	local box_data = read_file(modpath .. "/exit.box")
	boxes.import_box(1, box_data)
end

if not db.box_exists(2) then
	local box_data = read_file(modpath .. "/box.box")
	boxes.import_box(2, box_data)
end

-- local worldpath = minetest.get_worldpath()
-- write_file(worldpath .. "/entry.box", boxes.export_box(0))
-- write_file(worldpath .. "/exit.box", boxes.export_box(1))
-- write_file(worldpath .. "/box.box", boxes.export_box(2))
