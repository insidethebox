
conf = {}

conf.tutorial = {}

local settings = minetest.settings

conf.tutorial.required = (settings:get("tutorial_required") or "true") == "true"
conf.tutorial.series = tonumber(settings:get("tutorial_series") or "1")
conf.tutorial.entry_lobby = tonumber(settings:get("tutorial_entry_lobby") or 0)
conf.tutorial.exit_lobby = tonumber(settings:get("tutorial_exit_lobby") or 1)
conf.tutorial.exit = minetest.string_to_pos(settings:get("tutorial_exit") or "(0,0,0)")

conf.lobby = {}

conf.lobby.respawn_center = minetest.string_to_pos(settings:get("lobby_respawn_center") or "(0,0,0)")
conf.lobby.respawn_radius = settings:get("lobby_respawn_radius") or "8,12"

