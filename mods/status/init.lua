
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

--
-- print server status (version, uptime, max_lag, clients) every 5 minutes
--

local function print_status()
	local clients = function()
		local s = ""
		for _, v in pairs(minetest.get_connected_players()) do
			if s ~= "" then s = s .. " " end
			s = s .. v:get_player_name()
		end
		return s
	end
	local clientcount = function()
		local c = 0
		for _, _ in pairs(minetest.get_connected_players()) do
			c = c + 1
		end
		return c
	end

	minetest.log("action",
		"|minetest server status report|" ..
		"version=\"" .. minetest.get_version().string .. "\", " ..
		"uptime=\"" .. math.floor(minetest.get_server_uptime() / 86400) .. "\", " ..
		"max_lag=\"" .. math.floor(minetest.get_server_max_lag() * 1000)/1000 .. "\", " ..
		"client_count=\"" .. clientcount() .. "\", " ..
		"clients=\"" .. clients() .. "\""
		)

	minetest.after(300, print_status)
end
minetest.after(10, print_status)

